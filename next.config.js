/* eslint-disable */
const withSourceMaps = require('@zeit/next-source-maps')();

const API_BASE = 'https://kawalcovid19-wp.herokuapp.com/wp/wp-json/';

module.exports = withSourceMaps({
  env: {
    WORDPRESS_API_BASE: process.env.WORDPRESS_API_BASE || API_BASE,
    SENTRY_DSN: process.env.SENTRY_DSN,
  },
});
